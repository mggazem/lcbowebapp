package sheridan;

import sheridan.LiquourType;

import java.util.ArrayList;
import java.util.List;

public class LiquourService {
	

    public List<String> getAvailableBrands(LiquourType type){

        List<String> brands = new ArrayList( );
        
        if(type.equals(LiquourType.Wine)){
            brands.add("Adrianna Vineyard");
            brands.add("Barefoot");
            brands.add("J. P. Chenet");
            brands.add("Spumante Bambino");
            brands.add("Yellowglen");
        }else if(type.equals(LiquourType.Whiskey)){
            brands.add("Buchanan's");
            brands.add("Glenfiddich");
            brands.add("Jack Daniel's");
            brands.add("Johnnie Walker");
            brands.add("Seagrams");
        }else if(type.equals(LiquourType.Beer)){
        	brands.add("Budweiser");
        	brands.add("Canadian");
        	brands.add("Coors");
            brands.add("Corona");
            brands.add("Heineken");

        }else {
            brands.add("No Brand Available");
        }
    return brands;
    }
}
